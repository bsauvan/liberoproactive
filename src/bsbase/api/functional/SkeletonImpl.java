package bsbase.api.functional;

import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.proactive.core.component.componentcontroller.AbstractPAComponentController;

public class SkeletonImpl extends AbstractPAComponentController implements
        SkeletonInItf, SkeletonOutItf, BindingController {

    private static final long serialVersionUID = 1L;
    private SkeletonOutItf worker = null;
    public static String ITF_CLIENT = "wout";
    long totalTime = 0L;
    int totalTaskNo = 0;
    long lastTime = 0L;

    public SkeletonImpl() {
    }

    @Override
    public void bindFc(String clientItfName, Object serverItf)
            throws NoSuchInterfaceException, IllegalBindingException,
            IllegalLifeCycleException {
        if (ITF_CLIENT.equals(clientItfName)) {
            this.worker = (SkeletonOutItf) serverItf;
        } else {
            throw new NoSuchInterfaceException(clientItfName);
        }
    }

    @Override
    public String[] listFc() {
        return new String[] {ITF_CLIENT};
    }

    @Override
    public Object lookupFc(String clientItfName)
            throws NoSuchInterfaceException {
        if (ITF_CLIENT.equals(clientItfName)) {
            return this.worker;
        } else {
            throw new NoSuchInterfaceException(clientItfName);
        }
    }

    @Override
    public void unbindFc(String clientItfName) throws NoSuchInterfaceException,
            IllegalBindingException, IllegalLifeCycleException {
        if (ITF_CLIENT.equals(clientItfName)) {
            this.worker = null;
        } else {
            throw new NoSuchInterfaceException(clientItfName);
        }
    }

    @Override
    public void computeIn(Object task) {
        System.out.println("[SkeletonImpl]computeIn");

        // take initial time
        long t0 = System.currentTimeMillis();

        // compute the task result
        Object result;
        // System.out.println("[SkeletonImpl]computeIn "+"Param recu  "+ task
        // +" "+ task.getClass().getName()+ "par" +this);

        result = this.compute(task);
        this.computeOut(result);

        // take final time
        long t1 = System.currentTimeMillis();

        // update statistics
        this.lastTime = t1 - t0;
        this.totalTime += this.lastTime;
        this.totalTaskNo++;

    }

    public Object compute(Object task) {
        System.out.println("[SkeletonImpl]compute");
        return "[SkeletonImpl]return of compute" + task;
    }

    @Override
    public void computeOut(Object task) {
        System.out.println("[SkeletonImpl]computeOut");
        this.worker.computeOut(task);
    }

}
