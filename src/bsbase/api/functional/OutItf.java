package bsbase.api.functional;

import java.util.List;

import org.objectweb.proactive.core.component.type.annotations.gathercast.MethodSynchro;

public interface OutItf {

    @MethodSynchro(waitForAll = false)
    public void computeOut(List<Object> result);

}
