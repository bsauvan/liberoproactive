package bsbase.api.functional;

import java.io.Serializable;

public interface SkeletonInItf extends Serializable {

    public void computeIn(Object task);

}