package bsbase.api.functional;

import java.util.List;

public class OutputComponentImpl implements OutItf {

    @Override
    public void computeOut(List<Object> result) {
        System.out.println("[OutputComponentImpl]computeOut");
        for (Object output : result) {
            System.out.println(output);
        }
    }

}
