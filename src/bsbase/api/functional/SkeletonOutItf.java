package bsbase.api.functional;

public interface SkeletonOutItf {

    public void computeOut(Object task);

}
