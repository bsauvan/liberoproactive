package bsbase.api.functional;

import java.util.List;

import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;

public class InputComponentImpl implements InItf, BindingController {

    private static final long serialVersionUID = 1L;
    private InItf in;
    public static String ITF_CLIENT = "in";

    @Override
    public void bindFc(String clientItfName, Object serverItf)
            throws NoSuchInterfaceException, IllegalBindingException,
            IllegalLifeCycleException {
        if (ITF_CLIENT.equals(clientItfName)) {
            this.in = (InItf) serverItf;
        } else {
            throw new NoSuchInterfaceException(clientItfName);
        }
    }

    @Override
    public String[] listFc() {
        return new String[] {ITF_CLIENT};
    }

    @Override
    public Object lookupFc(String clientItfName)
            throws NoSuchInterfaceException {
        if (ITF_CLIENT.equals(clientItfName)) {
            return this.in;
        } else {
            throw new NoSuchInterfaceException(clientItfName);
        }
    }

    @Override
    public void unbindFc(String clientItfName) throws NoSuchInterfaceException,
            IllegalBindingException, IllegalLifeCycleException {
        if (ITF_CLIENT.equals(clientItfName)) {
            this.in = null;
        } else {
            throw new NoSuchInterfaceException(clientItfName);
        }
    }

    @Override
    public void computeIn(List<Object> task) {
        task.add("a");
        task.add("b");
        task.add("c");
        this.in.computeIn(task);
    }

}
