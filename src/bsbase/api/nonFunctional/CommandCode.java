package bsbase.api.nonFunctional;

public enum CommandCode {

    INCREASE_PARALLELISM, // Increase parallelism degree
    DECREASE_PARALLELISM
    // Decrease parallelism degree

}
