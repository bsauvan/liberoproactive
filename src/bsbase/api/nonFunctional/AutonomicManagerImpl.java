package bsbase.api.nonFunctional;

import java.io.Serializable;
import java.io.StringReader;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;

import org.drools.KnowledgeBase;
import org.drools.KnowledgeBaseFactory;
import org.drools.builder.KnowledgeBuilder;
import org.drools.builder.KnowledgeBuilderFactory;
import org.drools.builder.ResourceType;
import org.drools.command.Command;
import org.drools.command.CommandFactory;
import org.drools.io.ResourceFactory;
import org.drools.runtime.StatelessKnowledgeSession;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.proactive.Body;
import org.objectweb.proactive.core.component.body.ComponentInitActive;
import org.objectweb.proactive.core.component.componentcontroller.AbstractPAComponentController;
import org.objectweb.proactive.core.component.control.PAMembraneController;

import bsbase.beans.NameValue;
import bsbase.util.Logs;

public class AutonomicManagerImpl extends AbstractPAComponentController
        implements ComponentInitActive, AutonomicManagerItf, Runnable,
        BindingController, Serializable {

    private static final long serialVersionUID = 1L;
    public final int _LIFE_CYCLE_MS = 500;
    private AutonomicManagerItf autonomicManagerItf;
    private AutonomicControllerItf amc;
    public static String ITF_CLIENT = "amc";
    private boolean active = false;
    private boolean running = false;
    private boolean contractChange = true;
    private ConcurrentHashMap<String, NameValue> variables =
            new ConcurrentHashMap<String, NameValue>();
    private String id = null;
    private String contract = "";
    private Thread myExec = null;
    private Component behaviouralSkeleton;
    private PAMembraneController memController;

    @Override
    public void setAutonomicManagerInterface(AutonomicManagerItf autonomicManagerItf) {
        this.autonomicManagerItf = autonomicManagerItf;
    }

    @Override
    public void setBehaviouralSkeleton(Component behaviouralSkeleton) {
        this.behaviouralSkeleton = behaviouralSkeleton;
    }

    @Override
    public void setMemController(PAMembraneController memController) {
        this.memController = memController;
    }

    public boolean isActive() {
        return this.active;
    }

    @Override
    public void bindFc(String clientItfName, Object serverItf)
            throws NoSuchInterfaceException, IllegalBindingException,
            IllegalLifeCycleException {
        if (ITF_CLIENT.equals(clientItfName)) {
            this.amc = (AutonomicControllerItf) serverItf;
        } else {
            throw new NoSuchInterfaceException(clientItfName);
        }
    }

    @Override
    public String[] listFc() {
        return new String[] {ITF_CLIENT};
    }

    @Override
    public Object lookupFc(String clientItfName)
            throws NoSuchInterfaceException {
        if (ITF_CLIENT.equals(clientItfName)) {
            return this.amc;
        } else {
            throw new NoSuchInterfaceException(clientItfName);
        }
    }

    @Override
    public void unbindFc(String clientItfName) throws NoSuchInterfaceException,
            IllegalBindingException, IllegalLifeCycleException {
        if (ITF_CLIENT.equals(clientItfName)) {
            this.amc = null;
        } else {
            throw new NoSuchInterfaceException(clientItfName);
        }
    }

    public AutonomicManagerImpl() {

    }

    @Override
    public boolean setContract(String contract) {
        System.out.println("[AMImpl]setContract");
        if (contract == null) {
            if (Logs.isLoggable(Level.WARNING)) {
                Logs.warning("Manager#" + this.id + " pasa in modalit� attiva.");
            }

            return false;
        }

        this.contractChange = true;
        this.contract = contract;

        if (this.getWorkingSession() == null) {
            if (Logs.isLoggable(Level.SEVERE)) {
                Logs.info("Manager#" + this.id + " Regole non valide.");
            }

            return false;
        }

        if (Logs.isLoggable(Level.INFO)) {
            Logs.info("Manager#" + this.id + " cambia il suo contratto.");
        }

        return true;

    }

    @Override
    public String getContract() {
        System.out.println("[AMImpl]getContract");
        return this.contract;
    }

    @Override
    public Integer getMeasure(String m) {
        System.out.println("[AMImpl]getMeasure");
        if (m == null) {
            if (Logs.isLoggable(Level.WARNING)) {
                Logs.warning("Manager#" + this.id + " pasa in modalit� attiva.");
            }

            return null;
        }

        return this.amc.getMeasure(m);

    }

    @Override
    public List<String> getWorkerId() {
        System.out.println("[AMImpl]getWorkerId");
        return this.amc.getWorkerId();

    }

    @Override
    public void setWorkerId(String id) {
        this.amc.setWorkerId(id);
    }

    public Component commitComponent(Component behaviouralSkeleton,
                                     PAMembraneController memController) {
        System.out.println("[AmImpl]commitComponent");
        return this.amc.commitComponent(behaviouralSkeleton, memController);

    }

    public void releaseComponent(Component behaviouralSkeleton, String workerId) {
        System.out.println("[AmImpl]releaseComponent");
        this.amc.releaseComponent(behaviouralSkeleton, workerId);

    }

    @Override
    public boolean bindWorkers() {
        return this.bindWorkers(this.behaviouralSkeleton);
    }

    public boolean bindWorkers(Component behaviouralSkeleton) {
        System.out.println("[AmImpl]bindWorkers");
        return this.amc.bindWorkers(behaviouralSkeleton);
    }

    @Override
    public boolean bindWorker(Component worker) {
        System.out.println("[AmImpl]bindWorker");
        return this.amc.bindWorker(this.behaviouralSkeleton, worker);
    }

    public boolean bindWorker(Component behaviouralSkeleton, Component worker) {
        System.out.println("[AmImpl]bindWorker");
        return this.amc.bindWorker(behaviouralSkeleton, worker);
    }

    @Override
    public void run() {
        System.out.println("RUN");
        StatelessKnowledgeSession session = null;

        if (Logs.isLoggable(Level.INFO)) {
            Logs.info("Manager#" + this.id + " passa in esecuzione.");
        }

        while (this.running) {
            synchronized (this.myExec) {
                try {
                    if (this.active == true) {
                        this.myExec.wait(this._LIFE_CYCLE_MS);
                    } else if (this.running) {
                        this.myExec.wait();
                    } else {
                        break;
                    }
                } catch (Exception e) {
                }
            }

            // TODO : find an equivalent
            // if (!running || bsController == null)
            // continue;

            // Code to be executed in the active part of the manager
            // Creates the session in the runtime as non serializable
            if (this.contractChange || session == null) {
                if (Logs.isLoggable(Level.INFO)) {
                    Logs.info("Manager#"
                            + this.id
                            + " Change the KnowledgeBase session within the manager's life cycle");
                }

                if (Logs.isLoggable(Level.FINER)) {
                    Logs.fine("Change the knowledgeBase session"
                            + "within the manager's life cycle#" + this.id
                            + ".");
                }

                session = this.getWorkingSession();

                this.contractChange = false;
            }

            Vector<Command<?>> cmd = new Vector<Command<?>>();

            cmd.add(CommandFactory.newInsert(this.amc));
            cmd.add(CommandFactory.newInsert(this.autonomicManagerItf));

            for (NameValue param : this.variables.values()) {
                cmd.add(CommandFactory.newInsert(param));
            }

            if (session == null) {
                if (Logs.isLoggable(Level.SEVERE)) {
                    Logs.severe("Manager#" + this.id
                            + " it's not possible to create the session.");
                }

                // try
                // {
                this.end();
                // }
                // catch (RemoteException e)
                // {}
                System.out.println("[AMImpl]run -session null");
                break;
            }
            session.execute(CommandFactory.newBatchExecution(cmd));
            System.out.println("[AMImpl]run - execute");
        }

        if (Logs.isLoggable(Level.FINER)) {
            Logs.fine("end of manager execution#" + this.id);
        }
        System.out.println("[AMImpl]fin de run");
    }

    private StatelessKnowledgeSession getWorkingSession() {
        KnowledgeBuilder kbuild = KnowledgeBuilderFactory.newKnowledgeBuilder();
        KnowledgeBase kbase = KnowledgeBaseFactory.newKnowledgeBase();

        if (this.contract == null) {
            return null;
        }

        kbuild.add(ResourceFactory.newReaderResource(new StringReader(
                this.contract)), ResourceType.DRL);

        if (kbuild.hasErrors()) {
            if (Logs.isLoggable(Level.SEVERE)) {
                Logs.severe("Manager#" + this.id
                        + " errore compilazione regole : "
                        + kbuild.getErrors().toString());
            }
            System.out.println("" + kbuild.getErrors().toString());
            return null;
        }

        kbase.addKnowledgePackages(kbuild.getKnowledgePackages());

        return kbase.newStatelessKnowledgeSession();
    }

    @Override
    public Object getContractParam(String varName) {
        System.out.println("[AMImpl]");
        return this.variables.get(varName).getValue();

    }

    @Override
    public void enterActive() {
        System.out.println("ENTER ACTIVE");
        if (Logs.isLoggable(Level.INFO)) {
            Logs.info("Manager#" + this.id + " enters active mode.");
        }

        this.active = true;

        if (this.myExec != null) {
            synchronized (this.myExec) {
                this.myExec.notify();
            }
        }
    }

    public void enterPassive() {
        if (Logs.isLoggable(Level.INFO)) {
            Logs.info("Manager#" + this.id + " enters passive mode.");
        }

        this.active = false;
    }

    @Override
    public boolean begin() {
        System.out.println("BEGIN");
        this.running = true;

        if (this.myExec == null) {
            if (this.getWorkingSession() == null) {
                if (Logs.isLoggable(Level.SEVERE)) {
                    Logs.info("Manager#" + this.id + " non valid rule.");
                }

                System.out.println("begin retourne FALSE");
                return false;
            }

            this.myExec = new Thread(this);

            this.enterActive();

            this.myExec.start();

        }

        return true;
    }

    @Override
    public boolean end() {
        System.out.println("[AmImpl]end");
        this.running = false;
        this.enterPassive();

        if (this.myExec != null) {
            synchronized (this.myExec) {
                this.myExec.notify();

                try {
                    this.myExec.join();
                } catch (InterruptedException e) {
                }
            }

        }

        this.myExec = null;

        return true;
    }

    @Override
    public void initComponentActivity(Body body) {
        body.setImmediateService("commitComponent", false);
        body.setImmediateService("getMeasure", false);

    }

    @Override
    public Component commitComponent() {
        return this.commitComponent(
                this.behaviouralSkeleton, this.memController);
    }

    @Override
    public void releaseComponent(String workerId) {
        this.releaseComponent(this.behaviouralSkeleton, workerId);
    }

}
