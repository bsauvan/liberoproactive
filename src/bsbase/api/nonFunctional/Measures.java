package bsbase.api.nonFunctional;

public class Measures {

    public final static String SERVICETIME = "BSServiceTimeMeasure";
    public final static String LASTAVAILABLESERVICETIME =
            "BSLastAvaialableServiceTimeMeasure";
    public final static String TASKNUMBER = "BSTaskNumber";
    public final static String TOTALWORKERS = "BSWorkersNumber";
    public final static String NEXT_AVAILABLE_MACHINE = "NextAvailableMachine";

}