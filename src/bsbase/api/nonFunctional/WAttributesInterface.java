package bsbase.api.nonFunctional;

import org.objectweb.fractal.api.control.AttributeController;

public interface WAttributesInterface extends AttributeController {

    public void setWorkerId(String id);

}
