package bsbase.api.nonFunctional;

import java.util.List;

import org.objectweb.fractal.api.Component;
import org.objectweb.proactive.core.component.control.PAMembraneController;

public interface AutonomicControllerItf {

    public Integer getMeasure(String measure);

    // public boolean executeOperation(CommandCode command, Object... params);
    public List<String> getWorkerId();

    public void setWorkerId(String id);

    public Component commitComponent(Component behaviouralSkeleton,
                                     PAMembraneController memController);

    public boolean bindWorkers(Component behaviouralSkeleton);

    public boolean bindWorker(Component behaviouralSkeleton, Component worker);

    public void releaseComponent(Component behaviouralSkeleton, String workerId);

}
