package bsbase.api.nonFunctional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.etsi.uri.gcm.api.type.GCMTypeFactory;
import org.etsi.uri.gcm.util.GCM;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.Type;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalContentException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.api.type.TypeFactory;
import org.objectweb.proactive.Body;
import org.objectweb.proactive.core.component.Constants;
import org.objectweb.proactive.core.component.ContentDescription;
import org.objectweb.proactive.core.component.ControllerDescription;
import org.objectweb.proactive.core.component.Utils;
import org.objectweb.proactive.core.component.body.ComponentInitActive;
import org.objectweb.proactive.core.component.componentcontroller.AbstractPAComponentController;
import org.objectweb.proactive.core.component.control.PAMembraneController;
import org.objectweb.proactive.core.component.exceptions.NoSuchComponentException;
import org.objectweb.proactive.core.component.factory.PAGenericFactory;
import org.objectweb.proactive.core.component.type.PAGCMTypeFactory;

import bsbase.api.functional.SkeletonImpl;
import bsbase.api.functional.SkeletonInItf;
import bsbase.api.functional.SkeletonOutItf;

public class AutonomicControllerImpl extends AbstractPAComponentController
        implements ComponentInitActive, AutonomicControllerItf,
        BindingController {

    private static final long serialVersionUID = 1L;
    private int numWorkers = 0;
    private int totalTaskNo = 0;
    private AccListItf acc;
    public static String ITF_CLIENT = "acc";
    private IdcItf idc;
    private int index = 0;
    public static String ITF_CLIENT_1 = "idc";
    private Map<String, Component> workers = new HashMap<String, Component>();

    @Override
    public void bindFc(String clientItfName, Object serverItf)
            throws NoSuchInterfaceException, IllegalBindingException,
            IllegalLifeCycleException {
        if (ITF_CLIENT.equals(clientItfName)) {
            this.acc = (AccListItf) serverItf;
        } else if (ITF_CLIENT_1.equals(clientItfName)) {
            this.idc = (IdcItf) serverItf;
        } else {
            throw new NoSuchInterfaceException(clientItfName);
        }
    }

    @Override
    public String[] listFc() {
        return new String[] {ITF_CLIENT, ITF_CLIENT_1};
    }

    @Override
    public Object lookupFc(String clientItfName)
            throws NoSuchInterfaceException {
        if (ITF_CLIENT.equals(clientItfName)) {
            return this.acc;
        } else if (ITF_CLIENT_1.equals(clientItfName)) {
            return this.idc;
        } else {
            throw new NoSuchInterfaceException(clientItfName);
        }
    }

    @Override
    public void unbindFc(String clientItfName) throws NoSuchInterfaceException,
            IllegalBindingException, IllegalLifeCycleException {
        if (ITF_CLIENT.equals(clientItfName)) {
            this.acc = null;
        } else if (ITF_CLIENT_1.equals(clientItfName)) {
            this.idc = null;
        } else {
            throw new NoSuchInterfaceException(clientItfName);
        }
    }

    @Override
    public Integer getMeasure(String measure) {

        if (measure.equals(Measures.TASKNUMBER)) {
            return new Integer(this.totalTaskNo);
        }

        if (measure.equals(Measures.TOTALWORKERS)) {
            return new Integer(this.numWorkers);
        } else {
            return null;
        }
    }

    @Override
    public Component commitComponent(Component behaviouralSkeleton,
                                     PAMembraneController memController) {
        System.out.println("[AcImpl]commitComponent");
        Component worker = null;
        try {

            GCM.getLifeCycleController(behaviouralSkeleton).stopFc();
            Component boot;

            boot = Utils.getBootstrapComponent();
            PAGCMTypeFactory tf =
                    (PAGCMTypeFactory) GCM.getGCMTypeFactory(boot);
            PAGenericFactory gf =
                    (PAGenericFactory) GCM.getGenericFactory(boot);

            // worker
            InterfaceType[] tWorker =
                    new InterfaceType[] {
                            tf.createFcItfType(
                                    "win", SkeletonInItf.class.getName(),
                                    TypeFactory.SERVER, TypeFactory.MANDATORY,
                                    TypeFactory.SINGLE),

                            tf.createFcItfType(
                                    "wout", SkeletonOutItf.class.getName(),
                                    TypeFactory.CLIENT, TypeFactory.MANDATORY,
                                    TypeFactory.SINGLE)};

            InterfaceType[] tNFWorker =
                    new InterfaceType[] {
                            tf.createGCMItfType(
                                    Constants.BINDING_CONTROLLER,
                                    org.objectweb.proactive.core.component.control.PABindingController.class.getName(),
                                    TypeFactory.SERVER, TypeFactory.MANDATORY,
                                    GCMTypeFactory.SINGLETON_CARDINALITY),

                            tf.createGCMItfType(
                                    Constants.SUPER_CONTROLLER,
                                    org.objectweb.proactive.core.component.control.PASuperController.class.getName(),
                                    TypeFactory.SERVER, TypeFactory.MANDATORY,
                                    GCMTypeFactory.SINGLETON_CARDINALITY),

                            tf.createGCMItfType(
                                    Constants.MEMBRANE_CONTROLLER,
                                    org.objectweb.proactive.core.component.control.PAMembraneController.class.getName(),
                                    TypeFactory.SERVER, TypeFactory.MANDATORY,
                                    GCMTypeFactory.SINGLETON_CARDINALITY),

                            tf.createGCMItfType(
                                    Constants.GATHERCAST_CONTROLLER,
                                    org.etsi.uri.gcm.api.control.GathercastController.class.getName(),
                                    TypeFactory.SERVER, TypeFactory.MANDATORY,
                                    GCMTypeFactory.SINGLETON_CARDINALITY),
                            tf.createGCMItfType(
                                    "non-functional-server-controller",
                                    AccItf.class.getName(), TypeFactory.SERVER,
                                    TypeFactory.MANDATORY,
                                    GCMTypeFactory.SINGLETON_CARDINALITY),
                            tf.createGCMItfType(
                                    "idc-controller", IdcItf.class.getName(),
                                    TypeFactory.SERVER, TypeFactory.MANDATORY,
                                    GCMTypeFactory.SINGLETON_CARDINALITY)

                    };

            this.index += 1;

            worker =
                    gf.newFcInstance(
                            tf.createFcType(tWorker, tNFWorker),

                            new ControllerDescription(
                                    "worker" + this.index, Constants.PRIMITIVE,
                                    !Constants.SYNCHRONOUS,
                                    Constants.WITHOUT_CONFIG_FILE),
                            new ContentDescription(SkeletonImpl.class.getName()));

            ContentController ccBehaviouralSkeleton =
                    GCM.getContentController(behaviouralSkeleton);

            Type tAutonomicController =
                    tf.createFcType(new InterfaceType[] {
                            tf.createFcItfType(
                                    "acc", AccItf.class.getName(),
                                    TypeFactory.SERVER, TypeFactory.MANDATORY,
                                    TypeFactory.SINGLE),
                            tf.createFcItfType(
                                    "attribute-controller",
                                    WAttributesInterface.class.getName(),
                                    TypeFactory.SERVER, TypeFactory.MANDATORY,
                                    TypeFactory.SINGLE),
                            tf.createFcItfType(
                                    "idc", IdcItf.class.getName(),
                                    TypeFactory.SERVER, TypeFactory.MANDATORY,
                                    TypeFactory.SINGLE),});

            Component wAutonomicController =
                    gf.newNfFcInstance(
                            tAutonomicController,
                            new ControllerDescription(
                                    "wAutonomicController" + this.index,
                                    Constants.PRIMITIVE,
                                    "/org/objectweb/proactive/core/component/componentcontroller/config/default-component-controller-config.xml"),
                            new ContentDescription(
                                    WAutonomicControllerImpl.class.getName()));

            PAMembraneController wMemController =
                    Utils.getPAMembraneController(worker);

            wMemController.setControllerObject(
                    Constants.BINDING_CONTROLLER,
                    org.objectweb.proactive.core.component.control.PABindingControllerImpl.class.getName());
            wMemController.setControllerObject(
                    Constants.SUPER_CONTROLLER,
                    org.objectweb.proactive.core.component.control.PASuperControllerImpl.class.getName());
            wMemController.setControllerObject(
                    Constants.GATHERCAST_CONTROLLER,
                    org.objectweb.proactive.core.component.control.PAGathercastControllerImpl.class.getName());

            // Add AutonomicController to each worker
            wMemController.nfAddFcSubComponent(wAutonomicController);

            // add worker to BS
            ccBehaviouralSkeleton.addFcSubComponent(worker);

            // NFBinding
            wMemController.nfBindFc(
                    "non-functional-server-controller", "wAutonomicController"
                            + this.index + ".acc");

            wMemController.nfBindFc("idc-controller", "wAutonomicController"
                    + this.index + ".idc");

            memController.stopMembrane();

            memController.nfBindFc(
                    "internal-interface-client-controller", "worker"
                            + this.index + ".non-functional-server-controller");

            memController.nfBindFc("internal-idc-controller", "worker"
                    + this.index + ".idc-controller");

            // start worker's membrane
            wMemController.startMembrane();
            String workerId = GCM.getNameController(worker).getFcName();
            this.setWorkerId(workerId);
            this.addworkerToList(worker, workerId);
            this.numWorkers++;
            memController.startMembrane();
            // WAttributesInterface attributesInterface = (WAttributesInterface)
            // GCM
            // .getAttributeController(wAutonomicController);
            // attributesInterface.setWorkerId(GCM.getNameController(
            // worker).getFcName());

        } catch (IllegalBindingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (NoSuchInterfaceException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (InstantiationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IllegalContentException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IllegalLifeCycleException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (NoSuchComponentException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return worker;
    }

    @Override
    public boolean bindWorkers(Component behaviouralSkeleton) {
        System.out.println("[AcImpl]bindWorkers");

        for (Component worker : this.workers.values()) {
            BindingController bcBehaviouralSkeleton;
            try {
                bcBehaviouralSkeleton =
                        GCM.getBindingController(behaviouralSkeleton);
                BindingController bcWorker = GCM.getBindingController(worker);
                bcBehaviouralSkeleton.bindFc("in", worker.getFcInterface("win"));

                bcWorker.bindFc(
                        "wout", behaviouralSkeleton.getFcInterface("out"));

            } catch (NoSuchInterfaceException e) {
                System.out.println("[AmImpl]NoSuchInterfaceException");
                e.printStackTrace();
                return false;

            } catch (IllegalBindingException e) {
                System.out.println("[AmImpl]IllegalBindingException");
                e.printStackTrace();
                return false;
            } catch (IllegalLifeCycleException e) {
                System.out.println("[AmImpl]IllegalLifeCycleException");
                e.printStackTrace();
                return false;
            }

        }
        return true;
    }

    @Override
    public boolean bindWorker(Component behaviouralSkeleton, Component worker) {
        System.out.println("[AcImpl]bindWorker");

        BindingController bcBehaviouralSkeleton;
        try {
            bcBehaviouralSkeleton =
                    GCM.getBindingController(behaviouralSkeleton);
            BindingController bcWorker = GCM.getBindingController(worker);
            bcBehaviouralSkeleton.bindFc("in", worker.getFcInterface("win"));

            bcWorker.bindFc("wout", behaviouralSkeleton.getFcInterface("out"));

        } catch (NoSuchInterfaceException e) {
            System.out.println("[AcImpl]NoSuchInterfaceException");
            e.printStackTrace();
            return false;
        } catch (IllegalBindingException e) {
            System.out.println("[AcImpl]IllegalBindingException");
            e.printStackTrace();
            return false;
        } catch (IllegalLifeCycleException e) {
            System.out.println("[AmImpl]IllegalLifeCycleException");
            e.printStackTrace();
            return false;
        }

        return true;
    }

    private boolean addworkerToList(Component worker, String id) {
        this.workers.put(id, worker);
        return true;
    }

    @SuppressWarnings("unchecked")
    public ArrayList<Component> getWorkersList() {
        return (ArrayList<Component>) this.workers;
    }

    @Override
    public List<String> getWorkerId() {
        return this.acc.getWorkerId();
    }

    @Override
    public void setWorkerId(String id) {
        this.idc.setWorkerId(id);
    }

    public Component getWorker(String workerId) {
        for (String id : this.workers.keySet()) {
            if (id == workerId) {
                return this.workers.get(id);
            }
        }
        return null;
    }

    @Override
    public void releaseComponent(Component behaviouralSkeleton, String workerId) {
        try {
            Component worker = this.getWorker(workerId);
            ContentController ccBehaviouralSkeleton =
                    GCM.getContentController(behaviouralSkeleton);
            ccBehaviouralSkeleton.removeFcSubComponent(worker);
            this.workers.remove(workerId);

        } catch (NoSuchInterfaceException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IllegalContentException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IllegalLifeCycleException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public void initComponentActivity(Body body) {
        body.setImmediateService("commitComponent", false);
        body.setImmediateService("getMeasure", false);
    }

}
