package bsbase.api.nonFunctional;

//import java.rmi.RemoteException;
import java.util.List;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.control.AttributeController;
import org.objectweb.proactive.core.component.control.PAMembraneController;

public interface AutonomicManagerItf extends AttributeController {

    String getContract();

    Object getContractParam(String varName);

    Integer getMeasure(String contract);

    boolean begin();

    boolean end();

    void enterActive();

    public boolean setContract(String contract);

    public Component commitComponent();

    public boolean bindWorkers();

    public boolean bindWorker(Component worker);

    public void setWorkerId(String id);

    public List<String> getWorkerId();

    public void setBehaviouralSkeleton(Component behaviouralSkeleton);

    public void setMemController(PAMembraneController memController);

    public void setAutonomicManagerInterface(AutonomicManagerItf autonomicManagerItf);

    public void releaseComponent(String workerId);

}
