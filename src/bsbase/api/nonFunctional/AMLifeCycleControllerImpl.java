package bsbase.api.nonFunctional;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.proactive.core.component.control.PAGCMLifeCycleControllerImpl;

public class AMLifeCycleControllerImpl extends PAGCMLifeCycleControllerImpl {

    private static final long serialVersionUID = 1L;

    public AMLifeCycleControllerImpl(Component owner) {
        super(owner);
        // TODO Auto-generated constructor stub
    }

    @Override
    public void startFc() {
        try {
            super.startFc();
            AutonomicManagerItf amItf =
                    (AutonomicManagerItf) this.owner.getFcInterface("am");
            amItf.begin();
        } catch (IllegalLifeCycleException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (NoSuchInterfaceException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

}
