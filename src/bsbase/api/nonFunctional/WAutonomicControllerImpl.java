package bsbase.api.nonFunctional;

import org.objectweb.proactive.core.component.componentcontroller.AbstractPAComponentController;

public class WAutonomicControllerImpl extends AbstractPAComponentController
        implements WAttributesInterface, IdcItf, AccItf {

    private static final long serialVersionUID = 1L;
    private String id;

    @Override
    public String getWorkerId() {
        return this.id;
    }

    @Override
    public void setWorkerId(String id) {
        this.id = id;
    }

}
