package bsbase;

import java.util.ArrayList;
import java.util.List;

import org.etsi.uri.gcm.api.type.GCMTypeFactory;
import org.etsi.uri.gcm.util.GCM;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Type;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.api.type.TypeFactory;
import org.objectweb.proactive.core.component.Constants;
import org.objectweb.proactive.core.component.ContentDescription;
import org.objectweb.proactive.core.component.ControllerDescription;
import org.objectweb.proactive.core.component.Utils;
import org.objectweb.proactive.core.component.control.PAMembraneController;
import org.objectweb.proactive.core.component.factory.PAGenericFactory;
import org.objectweb.proactive.core.component.type.PAGCMTypeFactory;

import bsbase.api.functional.InItf;
import bsbase.api.functional.InputComponentImpl;
import bsbase.api.functional.OutItf;
import bsbase.api.functional.OutputComponentImpl;
import bsbase.api.nonFunctional.AccListItf;
import bsbase.api.nonFunctional.AutonomicControllerImpl;
import bsbase.api.nonFunctional.AutonomicControllerItf;
import bsbase.api.nonFunctional.AutonomicManagerImpl;
import bsbase.api.nonFunctional.AutonomicManagerItf;
import bsbase.api.nonFunctional.IdcItf;
import bsbase.util.ContractReader;

public class Main {

    public static void main(String[] args) throws Exception {
        Component boot = Utils.getBootstrapComponent();
        PAGCMTypeFactory tf = (PAGCMTypeFactory) GCM.getGCMTypeFactory(boot);
        PAGenericFactory gf = (PAGenericFactory) GCM.getGenericFactory(boot);

        // Application
        ComponentType tApplication = tf.createFcType(new InterfaceType[] {});
        Component application =
                gf.newFcInstance(tApplication, new ControllerDescription(
                        "application", Constants.COMPOSITE), null);

        // BehaviouralSkeleton
        InterfaceType[] tFBehavourialSkeleton =
                new InterfaceType[] {
                        tf.createGCMItfType(
                                "in", InItf.class.getName(),
                                TypeFactory.SERVER, TypeFactory.OPTIONAL,
                                GCMTypeFactory.MULTICAST_CARDINALITY),
                        tf.createGCMItfType(
                                "out", OutItf.class.getName(),
                                TypeFactory.CLIENT, TypeFactory.OPTIONAL,
                                GCMTypeFactory.GATHERCAST_CARDINALITY)};
        InterfaceType[] tNFBehavourialSkeleton =
                new InterfaceType[] {
                        tf.createGCMItfType(
                                "autonomicManager-server-controller",
                                AutonomicManagerItf.class.getName(),
                                TypeFactory.SERVER, TypeFactory.MANDATORY,
                                GCMTypeFactory.SINGLETON_CARDINALITY),
                        tf.createGCMItfType(
                                Constants.BINDING_CONTROLLER,
                                org.objectweb.proactive.core.component.control.PABindingController.class.getName(),
                                TypeFactory.SERVER, TypeFactory.MANDATORY,
                                GCMTypeFactory.SINGLETON_CARDINALITY),
                        tf.createGCMItfType(
                                Constants.CONTENT_CONTROLLER,
                                org.objectweb.proactive.core.component.control.PAContentController.class.getName(),
                                TypeFactory.SERVER, TypeFactory.MANDATORY,
                                GCMTypeFactory.SINGLETON_CARDINALITY),
                        tf.createGCMItfType(
                                Constants.SUPER_CONTROLLER,
                                org.objectweb.proactive.core.component.control.PASuperController.class.getName(),
                                TypeFactory.SERVER, TypeFactory.MANDATORY,
                                GCMTypeFactory.SINGLETON_CARDINALITY),
                        tf.createGCMItfType(
                                Constants.MEMBRANE_CONTROLLER,
                                org.objectweb.proactive.core.component.control.PAMembraneController.class.getName(),
                                TypeFactory.SERVER, TypeFactory.MANDATORY,
                                GCMTypeFactory.SINGLETON_CARDINALITY),
                        tf.createGCMItfType(
                                Constants.MULTICAST_CONTROLLER,
                                org.objectweb.proactive.core.component.control.PAMulticastController.class.getName(),
                                TypeFactory.SERVER, TypeFactory.MANDATORY,
                                GCMTypeFactory.SINGLETON_CARDINALITY),
                        tf.createGCMItfType(
                                Constants.GATHERCAST_CONTROLLER,
                                org.etsi.uri.gcm.api.control.GathercastController.class.getName(),
                                TypeFactory.SERVER, TypeFactory.MANDATORY,
                                GCMTypeFactory.SINGLETON_CARDINALITY),
                        tf.createGCMItfType(
                                "internal-interface-client-controller",
                                AccListItf.class.getName(), TypeFactory.CLIENT,
                                TypeFactory.MANDATORY,
                                GCMTypeFactory.MULTICAST_CARDINALITY,
                                PAGCMTypeFactory.INTERNAL),
                        tf.createGCMItfType(
                                "internal-idc-controller",
                                IdcItf.class.getName(), TypeFactory.CLIENT,
                                TypeFactory.OPTIONAL,
                                GCMTypeFactory.SINGLETON_CARDINALITY,
                                PAGCMTypeFactory.INTERNAL)};

        Component behaviouralSkeleton =
                gf.newFcInstance(
                        tf.createFcType(
                                tFBehavourialSkeleton, tNFBehavourialSkeleton),
                        new ControllerDescription(
                                "behaviouralSkeleton", Constants.COMPOSITE,
                                !Constants.SYNCHRONOUS,
                                Constants.WITHOUT_CONFIG_FILE),
                        (ContentDescription) null);

        // InputComponent
        ComponentType tInputComponent =
                tf.createFcType(new InterfaceType[] {
                        tf.createFcItfType(
                                "input", InItf.class.getName(),
                                TypeFactory.SERVER, TypeFactory.MANDATORY,
                                TypeFactory.SINGLE),
                        tf.createFcItfType(
                                "in", InItf.class.getName(),
                                TypeFactory.CLIENT, TypeFactory.OPTIONAL,
                                TypeFactory.SINGLE)});
        Component inputComponent =
                gf.newFcInstance(
                        tInputComponent, new ControllerDescription(
                                "inputComponent", Constants.PRIMITIVE),
                        InputComponentImpl.class.getName());

        // OutputComponent
        ComponentType tOutputComponent =
                tf.createFcType(new InterfaceType[] {tf.createFcItfType(
                        "out", OutItf.class.getName(), TypeFactory.SERVER,
                        TypeFactory.OPTIONAL, TypeFactory.SINGLE)});
        Component outputComponent =
                gf.newFcInstance(
                        tOutputComponent, new ControllerDescription(
                                "outputComponent", Constants.PRIMITIVE),
                        OutputComponentImpl.class.getName());

        // AutonomicManager
        Type tAutonomicManager =
                tf.createFcType(new InterfaceType[] {
                        tf.createFcItfType(
                                "am", AutonomicManagerItf.class.getName(),
                                TypeFactory.SERVER, TypeFactory.MANDATORY,
                                TypeFactory.SINGLE),
                        tf.createFcItfType(
                                "amc", AutonomicControllerItf.class.getName(),
                                TypeFactory.CLIENT, TypeFactory.MANDATORY,
                                TypeFactory.SINGLE),
                        tf.createFcItfType(
                                "attribute-controller",
                                AutonomicManagerItf.class.getName(),
                                TypeFactory.SERVER, TypeFactory.MANDATORY,
                                TypeFactory.SINGLE),});

        Component autonomicManager =
                gf.newNfFcInstance(
                        tAutonomicManager,
                        new ControllerDescription(
                                "autonomicManager", Constants.PRIMITIVE,
                                "/bsbase/api/config/component-controller-config.xml"),
                        new ContentDescription(
                                AutonomicManagerImpl.class.getName()));

        // BehaviouralSkeleton's membrane
        PAMembraneController memController =
                Utils.getPAMembraneController(behaviouralSkeleton);

        // Use attribute controller to set contract
        AutonomicManagerItf attributesInterface =
                (AutonomicManagerItf) GCM.getAttributeController(autonomicManager);
        attributesInterface.setBehaviouralSkeleton(behaviouralSkeleton);
        attributesInterface.setMemController(memController);
        attributesInterface.setAutonomicManagerInterface(attributesInterface);
        attributesInterface.setContract(ContractReader.readContractFromDrl(System.getProperty("project.home")
                + "/resources/Rules" + ".drl"));

        // AutonomicController
        Type tAutonomicController =
                tf.createFcType(new InterfaceType[] {
                        tf.createFcItfType(
                                "amc", AutonomicControllerItf.class.getName(),
                                TypeFactory.SERVER, TypeFactory.MANDATORY,
                                TypeFactory.SINGLE),
                        tf.createFcItfType(
                                "acc", AccListItf.class.getName(),
                                TypeFactory.CLIENT, TypeFactory.OPTIONAL,
                                TypeFactory.SINGLE),
                        tf.createFcItfType(
                                "idc", IdcItf.class.getName(),
                                TypeFactory.CLIENT, TypeFactory.OPTIONAL,
                                TypeFactory.SINGLE),});
        Component autonomicController =
                gf.newNfFcInstance(
                        tAutonomicController,
                        new ControllerDescription(
                                "autonomicController",
                                Constants.PRIMITIVE,
                                "/org/objectweb/proactive/core/component/componentcontroller/config/default-component-controller-config.xml"),
                        new ContentDescription(
                                AutonomicControllerImpl.class.getName()));

        // BehaviouralSkeleton's membrane
        memController.setControllerObject(
                Constants.BINDING_CONTROLLER,
                org.objectweb.proactive.core.component.control.PABindingControllerImpl.class.getName());
        memController.setControllerObject(
                Constants.CONTENT_CONTROLLER,
                org.objectweb.proactive.core.component.control.PAContentControllerImpl.class.getName());
        memController.setControllerObject(
                Constants.SUPER_CONTROLLER,
                org.objectweb.proactive.core.component.control.PASuperControllerImpl.class.getName());

        memController.setControllerObject(
                Constants.MULTICAST_CONTROLLER,
                org.objectweb.proactive.core.component.control.PAMulticastControllerImpl.class.getName());

        memController.setControllerObject(
                Constants.GATHERCAST_CONTROLLER,
                org.objectweb.proactive.core.component.control.PAGathercastControllerImpl.class.getName());

        // Add AM and AC in BS's membrane + bindings
        System.out.println("Adding components into the membrane");
        memController.nfAddFcSubComponent(autonomicManager);
        memController.nfAddFcSubComponent(autonomicController);

        memController.nfBindFc(
                "autonomicManager-server-controller", "autonomicManager.am");
        memController.nfBindFc(
                "autonomicManager.amc", "autonomicController.amc");

        memController.nfBindFc(
                "autonomicController.acc",
                "internal-interface-client-controller");

        memController.nfBindFc(
                "autonomicController.idc", "internal-idc-controller");

        // Add BS, InputComponent and OutputComponent to Application
        ContentController ccApplication = GCM.getContentController(application);
        ccApplication.addFcSubComponent(behaviouralSkeleton);
        ccApplication.addFcSubComponent(inputComponent);
        ccApplication.addFcSubComponent(outputComponent);

        // Start membrane
        memController.startMembrane();

        // Do the bindings
        BindingController bcInputComponent =
                GCM.getBindingController(inputComponent);

        bcInputComponent.bindFc("in", behaviouralSkeleton.getFcInterface("in"));
        BindingController bcBehaviouralSkeleton =
                GCM.getBindingController(behaviouralSkeleton);

        attributesInterface.bindWorkers();

        bcBehaviouralSkeleton.bindFc(
                "out", outputComponent.getFcInterface("out"));

        Thread.sleep(10000);

        // Start application
        GCM.getGCMLifeCycleController(application).startFc();

        List<String> wIds = attributesInterface.getWorkerId();
        for (String wId : wIds) {
            System.out.println(" wIds = " + wId);
        }

        System.out.println("BS state : "
                + GCM.getGCMLifeCycleController(behaviouralSkeleton)
                        .getFcState());
        System.out.println("InputComponent state : "
                + GCM.getGCMLifeCycleController(inputComponent).getFcState());

        InItf input = (InItf) inputComponent.getFcInterface("input");
        List<Object> task = new ArrayList<Object>();
        input.computeIn(task);

        Thread.sleep(6000);

        // Stop application
        GCM.getGCMLifeCycleController(application).stopFc();
        GCM.getGCMLifeCycleController(application).terminateGCMComponent();
        System.exit(0);
    }

}
