package bsbase.beans;

public class ContractParamValue implements NameValue {

    String name = null;
    Object value = null;

    public ContractParamValue(String name, Object value) {
        this.name = name;
        this.value = value;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public Object getValue() {
        return this.value;
    }

    @Override
    public boolean setValue(Object value) {
        if (value == null) {
            return false;
        }

        this.value = value;

        return true;
    }

}