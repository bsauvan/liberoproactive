package bsbase.beans;

public class ObservedMeasure implements NameValue {

    String name = null;
    Object value = null;

    public ObservedMeasure(String name, Object value) {
        this.name = name;
        this.value = value;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public Object getValue() {
        return this.value;
    }

    @Override
    public boolean setValue(Object value) {
        if (value == null) {
            return false;
        }

        this.value = value;

        return true;
    }

}