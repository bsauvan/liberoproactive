package bsbase.beans;

public class MulticoncernBroadcastCodes {

    public final static String PREPARE_BROADCAST_COMMAND = "BROADCAST_PREPARE";
    public final static String BROADCAST_REQUEST = "BROADCAST_REQUEST";
    public final static String BROADCAST_REQUEST_WAIT_ACK = "WAIT_ACK";
    public final static String BROADCAST_PARAM = "BROADCAST_PARAM";
    public final static String BROADCAST_SECOND_PARAM =
            "BROADCAST_SECOND_PARAM";

    public final static String BROADCAST_RESPONSE_BASE = "BROADCAST_RESPONSE";

    public final static String BROADCAST_RESPONSE_OK_WITH_SECURITY =
            BROADCAST_RESPONSE_BASE + "_OK_ADD_SECURITY";
    public final static String BROADCAST_RESPONSE_OK = BROADCAST_RESPONSE_BASE
            + "_OK";
    public final static String BROADCAST_RESPONSE_NO = BROADCAST_RESPONSE_BASE
            + "_NO";

}