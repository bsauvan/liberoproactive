package bsbase.beans;

public interface NameValue {

    public abstract String getName();

    public abstract Object getValue();

    public abstract boolean setValue(Object value);

}