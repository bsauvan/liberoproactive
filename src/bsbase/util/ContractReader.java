package bsbase.util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * This class is used to read a conctract from a file, in such a way it may be
 * made available as a String.
 * <p>
 * 
 * Classe che legge un contratto da un file, e lo rende reperibile sotto forma
 * di stringa.
 * 
 * @author Vamis Xhagjika
 */
public class ContractReader {

    static public String readContractFromDrl(String fname) throws IOException {
        StringBuilder fileData = new StringBuilder();
        BufferedReader reader = new BufferedReader(new FileReader(fname));

        char[] buf = new char[1024];
        int numRead = 0;

        while ((numRead = reader.read(buf)) != -1) {
            fileData.append(buf, 0, numRead);
        }

        reader.close();
        return fileData.toString();
    }

}
