package bsbase.util;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;

//import remote.LookupClientMultiCastDiscovery;

/**
 * Static logggin class (process level). Generates a unique file for each
 * machine. Make the <code>Logger</code> Java class a Singleton.
 * <p>
 * Classe di logging statica a livello di processo. Genera un solo file per
 * macchina. Rende Singleton la classe <code>Logger</code> di java.
 * 
 * @author Vamis Xhagjika
 */
public class Logs {

    public static Level myLevel = Level.WARNING;
    public static DateFormat frm = new SimpleDateFormat(
            "yyyy-MM-dd HH:mm:ss:SSS");
    public static String thisMachine = null;

    public synchronized static void fine(String arg0) {
        log(Level.FINE, arg0, null);
    }

    public synchronized static void finer(String arg0) {
        log(Level.FINER, arg0, null);
    }

    public synchronized static void finest(String arg0) {
        log(Level.FINEST, arg0, null);
    }

    public synchronized static Level getLevel() {
        return myLevel;
    }

    public synchronized static void info(String arg0) {
        log(Level.INFO, arg0, null);
    }

    public synchronized static boolean isLoggable(Level arg0) {
        return arg0.intValue() >= myLevel.intValue();
    }

    public synchronized static void log(Level arg0, String arg1, Object arg2) {
        if (!isLoggable(arg0)) {
            return;
        }

        String message;
        Date tmp = new Date();

        if (thisMachine == null) {
            try {
                thisMachine = InetAddress.getLocalHost().getHostAddress();
            } catch (UnknownHostException e) {
            }
        }

        message =
                "[" + thisMachine + "@ " + frm.format(tmp)
                        + "]#[server@ %s] -:- " + arg1;

        if (arg2 != null) {
            message += " PARAMETER:" + arg2;
        }

        // if (!LookupClientMultiCastDiscovery.log(message + "\r\n"))
        // System.out.println(message + "\r\n");
    }

    public synchronized static void log(Level arg0, String arg1) {
        log(arg0, arg1, null);
    }

    public synchronized static void severe(String arg0) {
        log(Level.SEVERE, arg0, null);
    }

    public synchronized static void warning(String arg0) {
        log(Level.WARNING, arg0, null);
    }

}