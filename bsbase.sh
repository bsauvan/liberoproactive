#!/bin/sh

echo
echo --- BSBASE -------------------------------------------------
echo

JAVA_HOME=${JAVA_HOME-NULL};
if [ "$JAVA_HOME" = "NULL" ]
then
	echo
	echo "The enviroment variable JAVA_HOME must be set the current jdk distribution"
	echo "installed on your computer."
	echo "Use "
	echo "    export JAVA_HOME=<the directory where is the JDK>"
	exit 127
fi

if [ -z "$PROJECT_HOME" ]
then
	workingDir=`dirname $0`
	PROJECT_HOME=$(cd $workingDir/./ || (echo "Broken project installation" ; exit 1) && echo $PWD)
fi
export PROJECT_HOME

CLASSPATH=.
CLASSPATH=$CLASSPATH:$PROJECT_HOME/lib/proactive/ProActive.jar
CLASSPATH=$CLASSPATH:$PROJECT_HOME/lib/drools/*
CLASSPATH=$CLASSPATH:$PROJECT_HOME/classes
CLASSPATH=$CLASSPATH:$PROJECT_HOME/src
export CLASSPATH

JAVACMD=$JAVA_HOME/bin/java" -Djava.security.manager \
	-Djava.security.policy=$PROJECT_HOME/resources/proactive.java.policy \
	-Dlog4j.configuration=file:$PROJECT_HOME/resources/proactive-log4j \
	-Dgcm.provider=org.objectweb.proactive.core.component.Fractive \
	-Dproject.home=$PROJECT_HOME \
	-Dproactive.configuration=$PROJECT_HOME/resources/ProActiveConfiguration.xml \
	-Xmx512m \
	-Dos=unix"
export JAVACMD

$JAVACMD bsbase.Main "$@"

echo
echo ------------------------------------------------------------
